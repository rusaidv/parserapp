﻿using AutoMapper;
using ParserApp.Models;
using ParserApp.ViewModels;

namespace ParserApp.MappingConfigurations
{
    public class NewsViewModelProfile : Profile
    {
        public NewsViewModelProfile()
        {			
            CreateMap<News, NewsViewModel>().ReverseMap();
        }
    }
}