﻿using Microsoft.Extensions.DependencyInjection;
using ParserApp.Services.NewsService;
using ParserApp.Services.ParserService;

namespace ParserApp.Extensions
{
    public static class AddServices
    {
        public static void Services(this IServiceCollection services)
        {
            services.AddTransient<IParserService, ParserService>();
            services.AddTransient<INewsService, NewsService>();
        }
    }
}