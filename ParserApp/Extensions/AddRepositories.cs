﻿using Microsoft.Extensions.DependencyInjection;
using ParserApp.Repositories.NewsRepositories;

namespace ParserApp.Extensions
{
    public static class AddRepositories
    {
        public static void Repositories(this IServiceCollection services)
        {
            services.AddTransient<INewsRepository, NewsRepository>();
        }
    }
}