﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using HtmlAgilityPack;
using ParserApp.ViewModels;

namespace ParserApp.Services.ParserService
{
    public class ParserService : IParserService
    {
        public List<NewsViewModel> ParseNews()
        {
            try
            {
                string mainLink = @"https://www.mk.ru/news/";

                HtmlWeb web = new HtmlWeb();
                var htmlDoc = web.Load(mainLink);
                var linkNodes = htmlDoc.DocumentNode.SelectNodes("//a[contains(@class,'news-listing__item-link')]");
                
                List<NewsViewModel> listOfNews = new List<NewsViewModel>();
                for (int i = 0; i < 30; i++)
                {
                    var innerLink = linkNodes[i].GetAttributeValue("href", "");
                    var document = web.Load(innerLink);
                    if (document != null)
                    {
                        var titleNode = document.DocumentNode.SelectSingleNode(".//h1[contains(@class, 'article__title')]");
                        if (titleNode != null)
                        {
                            var newsTitle = titleNode.InnerText;
                    
                            var newsTextNode = document.DocumentNode.SelectSingleNode(".//div[contains(@class, 'article__body')]");
                            if (newsTextNode != null)
                            {
                                var newsText = newsTextNode.InnerText;
                                newsText = Regex.Replace(newsText, @"( |\t|\r?\n)\1+", "");
                                newsText = Regex.Replace(newsText, @"<[^>]+>|&nbsp;", "").Trim();
                                newsText = HttpUtility.HtmlDecode(newsText);
                    
                                var dateTimeNode = document.DocumentNode.SelectSingleNode(".//time[contains(@class, 'meta__text')]");
                                var newsTimeString = dateTimeNode.Attributes["datetime"].Value;
                                DateTime newsDateTime = Convert.ToDateTime(newsTimeString);
                                
                                NewsViewModel newsViewModel = new NewsViewModel()
                                {
                                    Title = newsTitle,
                                    Text = newsText,
                                    Time = newsDateTime
                                };
                                listOfNews.Add(newsViewModel);
                            }
                        }
                    }
                }
                return listOfNews;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }
        }

        public Task<List<NewsViewModel>> ParseNewsAsync()
        {
            return Task.Run(ParseNews);
        }
    }
}