﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ParserApp.ViewModels;

namespace ParserApp.Services.ParserService
{
    public interface IParserService
    {
        List<NewsViewModel> ParseNews();
        Task<List<NewsViewModel>> ParseNewsAsync();
    }
}