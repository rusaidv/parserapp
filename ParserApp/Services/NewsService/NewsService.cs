﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ParserApp.Models;
using ParserApp.Repositories.NewsRepositories;

namespace ParserApp.Services.NewsService
{
    public class NewsService : INewsService
    {
        private readonly INewsRepository _newsRepository;

        public NewsService(INewsRepository newsRepository)
        {
            _newsRepository = newsRepository;
        }

        public async Task<List<News>> GetNewsBetweenDates(DateTime dateFrom, DateTime dateTo)
        {
            IQueryable<News> newsList = _newsRepository.GetNews();
            return await newsList.Where(n => n.Time >= dateFrom && n.Time <= dateTo).ToListAsync();
        }
        
        public List<News> GetNewsByText(string text)
        {
            IQueryable<News> newsList = _newsRepository.GetNews();
            return newsList.Where(n => n.Text.Contains(text)).ToList();
        }
        
        public async Task<List<News>> GetNewsListWithoutDuplicates(List<News> newsList)
        {
            List<News> newsListWithoutDuplicates = new List<News>();
            foreach (var news in newsList)
            {
                if (!await _newsRepository.IsNewsExistInDatabase(news))
                {
                    newsListWithoutDuplicates.Add(news);
                }
            }

            return newsListWithoutDuplicates;
        }
        
        public async Task<List<string>> GetTopTenWordsFromNews()
        {
            List<News> newsList = await _newsRepository.GetNewsAsync();
            string text = String.Empty;
            foreach (var news in newsList)
            {
                text = text + " " + news.Text;
            }
            
            string newsText = text.Trim()
                .Replace(".",String.Empty)
                .Replace(",", String.Empty);

            var results = newsText
                .Split(' ').Where(x => x.Length > 3)
                .GroupBy(x => x)
                .Select(x => new
                {
                    KeyField = x.Key,
                    Count = x.Count()
                })
                .OrderByDescending(x => x.Count)
                .Take(10).Select(x => x.KeyField).ToList();
            
            return results;
        }
    }
}