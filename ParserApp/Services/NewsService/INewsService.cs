﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ParserApp.Models;

namespace ParserApp.Services.NewsService
{
    public interface INewsService
    {
        Task<List<News>> GetNewsBetweenDates(DateTime dateFrom, DateTime dateTo);
        List<News> GetNewsByText(string text);
        Task<List<News>> GetNewsListWithoutDuplicates(List<News> newsList);
        Task<List<string>> GetTopTenWordsFromNews();
    }
}