﻿using System;

namespace ParserApp.ViewModels
{
    public class NewsViewModel
    {
        public string Title { get; set; }
        public string Text { get; set; }
        public DateTime Time { get; set; }
    }
}