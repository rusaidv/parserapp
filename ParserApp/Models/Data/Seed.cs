﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace ParserApp.Models.Data
{
    public class Seed
    {
        public static async Task SeedData(DataContext context, UserManager<AppUser> userManager)
        {
            if (!userManager.Users.Any())
            {
                var user = new AppUser()
                {
                    UserName = "admin",
                    Email = "test@test.com"
                };
                await userManager.CreateAsync(user, "Pa$$w0rd");
            }
        }
    }
}