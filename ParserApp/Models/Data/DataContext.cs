﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace ParserApp.Models.Data
{
    public class DataContext : IdentityDbContext<AppUser>
    {
        public DbSet<News> News { get; set; }
        public DataContext(DbContextOptions options) : base(options)
        {
            
        }
       
    }
}