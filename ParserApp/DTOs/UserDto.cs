﻿namespace ParserApp.DTOs
{
    public class UserDto
    {
        public string Token { get; set; }
    }
}