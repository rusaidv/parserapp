﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ParserApp.DTOs;
using ParserApp.Models;
using ParserApp.Services.TokenService;

namespace ParserApp.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AccountController : ControllerBase
    {
        private readonly UserManager<AppUser> _userManager;
        private readonly SignInManager<AppUser> _signInManager;
        private readonly TokenService _tokenService;
        private readonly ILogger<AccountController> _logger;

        public AccountController(UserManager<AppUser> userManager,
            SignInManager<AppUser> signInManager,
            ILogger<AccountController> logger,
            TokenService tokenService)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _tokenService = tokenService;
            _logger = logger;
        }

        [HttpPost("login")]
        public async Task<ActionResult<UserDto>> Login(LoginDto loginDto)
        {
            try
            {
                _logger.LogInformation($"Пользователь {loginDto.Email} пытается авторизироваться");
                var user = await _userManager.FindByEmailAsync(loginDto.Email);
                if (user == null) return Unauthorized();
                var result = await _signInManager.CheckPasswordSignInAsync(user, loginDto.Password, false);
                if (result.Succeeded)
                {
                    _logger.LogInformation($"Пользователь {loginDto.Email} успешно авторизировался");
                    return new UserDto
                    {
                        Token = _tokenService.CreateToken(user)
                    };
                }
                _logger.LogWarning($"Авторизация пользователя {loginDto.Email} прошла не успешно");

                return Unauthorized();
            }
            catch (Exception e)
            {
                _logger.LogError($"Произошла ошибка в методе Login");
                return StatusCode(500);
            }
        }
    }
}