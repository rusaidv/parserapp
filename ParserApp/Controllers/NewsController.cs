﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ParserApp.Models;
using ParserApp.Repositories.NewsRepositories;
using ParserApp.Services.NewsService;
using ParserApp.Services.ParserService;
using ParserApp.ViewModels;

namespace ParserApp.Controllers
{
    [ApiController]
    [Route("api")]
    public class NewsController : ControllerBase
    {
        private readonly INewsRepository _newsRepository;
        private readonly ILogger<NewsController> _logger;
        private readonly IParserService _parserService;
        private readonly INewsService _newsService;
        private readonly IMapper _mapper;

        public NewsController(INewsRepository newsRepository,
            ILogger<NewsController> logger,
            IParserService parserService, 
            INewsService newsService, 
            IMapper mapper)
        {
            _newsRepository = newsRepository;
            _logger = logger;
            _parserService = parserService;
            _newsService = newsService;
            _mapper = mapper;
        }
        
        
        [HttpGet("parsenews")]
        [Authorize]
        public async Task<ActionResult> ParseNews()
        {
            try
            {
                _logger.LogInformation("Пришел запрос на парсинг новостей");
                List<NewsViewModel> newsViewModels = await _parserService.ParseNewsAsync();
                if (newsViewModels != null)
                {
                    List<News> news = _mapper.Map<List<NewsViewModel>, List<News>>(newsViewModels);
                    List<News> newsListWithoutDuplicates = await _newsService.GetNewsListWithoutDuplicates(news);
                    if (newsListWithoutDuplicates != null)
                    {
                        await _newsRepository.AddRangeAsync(newsListWithoutDuplicates);
                        await _newsRepository.SaveAsync();
                        _logger.LogInformation("Новости успешно спарсились");
                        return Ok();
                    }
                }
                return NotFound();
            }
            catch (Exception e)
            {
                _logger.LogError($"Произошла ошибка в методе ParseNews:\n {e}");
                return StatusCode(500);
            }
        }
        
        [HttpGet("posts")]
        [Authorize]
        public async Task<ActionResult<List<NewsViewModel>>> GetNewsBetweenDates(DateTime from, DateTime to)
        {
            try
            {
                _logger.LogInformation("Пришел запрос по поиску новостей по дате");
                List<News> newsList = await _newsService.GetNewsBetweenDates(from, to);
                List<NewsViewModel> newsViewModelList = _mapper.Map<List<News>, List<NewsViewModel>>(newsList);
                return newsViewModelList;
            }
            catch (Exception e)
            {
                _logger.LogError($"Произошла ошибка в методе GetNewsBetweenDates:\n {e}");
                return StatusCode(500);
            }
        }
        
        [HttpGet("search")]
        [Authorize]
        public ActionResult<List<NewsViewModel>> SearchNewsByText(string text)
        {
            try
            {
                _logger.LogInformation($"Пришел запрос поиска новостей по тексту");
                List<News> newsList = _newsService.GetNewsByText(text);
                List<NewsViewModel> newsViewModelList = _mapper.Map<List<News>, List<NewsViewModel>>(newsList);
                return newsViewModelList;
            }
            catch (Exception e)
            {
                _logger.LogError($"Произошла ошибка в методе SearchNewsByText:\n {e}");
                return StatusCode(500);
            }
        }
        
        [HttpGet("topten")]
        [Authorize]
        public async Task<ActionResult<List<string>>> GetTopTenWords()
        {
            try
            {
                _logger.LogInformation($"Пришел запрос по поиску топ 10 слов");
                List<string> topTenWordsFromNews = await _newsService.GetTopTenWordsFromNews();
                return topTenWordsFromNews;
            }
            catch (Exception e)
            {
                _logger.LogError($"Произошла ошибка в методе SearchNewsByText:\n {e}");
                return StatusCode(500);
            }
        }
    }
}