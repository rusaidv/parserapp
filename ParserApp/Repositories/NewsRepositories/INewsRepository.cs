﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ParserApp.Models;

namespace ParserApp.Repositories.NewsRepositories
{
    public interface INewsRepository
    { 
        Task SaveAsync();
        Task AddRangeAsync(List<News> items);
        IQueryable<News> GetNews();
        Task<List<News>> GetNewsAsync();
        Task<bool> IsNewsExistInDatabase(News news);
    }
}