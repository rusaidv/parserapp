﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ParserApp.Models;
using ParserApp.Models.Data;

namespace ParserApp.Repositories.NewsRepositories
{
    public class NewsRepository : INewsRepository
    {
        private readonly DataContext _context;
        
        public NewsRepository(DataContext context)
        {
            _context = context;
        }
        public async Task SaveAsync()
        {
            await _context.SaveChangesAsync();
        }
        public async Task AddRangeAsync(List<News> items)
        {
           await _context.AddRangeAsync(items);
        }
        public IQueryable<News> GetNews()
        {
            return _context.News;
        }
        
        public async Task<List<News>> GetNewsAsync()
        {
            return await _context.News.ToListAsync();
        }

        public async Task<bool> IsNewsExistInDatabase(News news)
        {
            return await _context.News.AnyAsync(n => n.Time == news.Time);
        }
    }
}