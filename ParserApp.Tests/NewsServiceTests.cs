using System;
using System.Collections.Generic;
using System.Linq;
using Moq;
using ParserApp.Models;
using ParserApp.Repositories.NewsRepositories;
using ParserApp.Services.NewsService;
using Xunit;

namespace ParserApp.Tests
{
    public class NewsServiceTests
    {
        private readonly NewsService _newsService;
        private readonly Mock<INewsRepository> _newsRepoMock;

        public NewsServiceTests()
        {
            _newsRepoMock = new Mock<INewsRepository>();
            _newsService = new NewsService(_newsRepoMock.Object);
        }

        [Fact]
        public void GetNewsListByText_Returns_List()
        {
            var text = "Токаев";
            
            var expectedList = new List<News>(){
                new News {
                    Title = "Токаев заявил о переломном моменте в истории Казахстана", 
                    Text = "В своем выступлении Касым-Жомарт Токаев подчеркнул, что геополитическая и геоэкономическая ситуация в мире является самой сложной за три последних десятилетия.",
                    Time = new DateTime(2022, 04, 10)
                },
                new News { 
                    Title = "Президент дополнил Указ", 
                    Text = "Президент страны Токаев поручил утвердить план дальнейших мер в сфере прав человека.",
                    Time = new DateTime(2022, 04, 10)  
                }
            };
            IQueryable<News> expected = expectedList.AsQueryable();
            
            _newsRepoMock.Setup(x => x.GetNews()).Returns(expected);
               
            List<News> actual = _newsService.GetNewsByText(text);
            
            Assert.NotEmpty(actual);
            Assert.Equal(expected,actual);
            Assert.True(expected.SequenceEqual(actual));
            Assert.All(actual,
                item =>  Assert.Contains("Токаев", item.Text)
            );
        }
       
    }
}